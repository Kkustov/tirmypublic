﻿using UnityEngine;

public class moveCam : MonoBehaviour
{
    public float speed, mnoj;
    private Vector2 startPos;
    private Camera cam;
    private float targetPos;
    public GameObject shopBGBonus;
    void Start()
    {
        cam=GetComponent<Camera>();
        targetPos = transform.position.x;
        
    }

    // Update is called once per frame
    void Update()
    {
        if (shopBGBonus.activeSelf == false){
        
        if(Input.GetMouseButtonDown(0)) startPos = cam.ScreenToWorldPoint(Input.mousePosition);
        else  if(Input.GetMouseButton(0))
        {
            float pos =cam.ScreenToWorldPoint(Input.mousePosition).x - startPos.x;
            targetPos = Mathf.Clamp(transform.position.x-pos*mnoj,-0.001f,6) ;

        }

      
        transform.position = new Vector3(Mathf.Lerp(transform.position.x,targetPos,speed*Time.deltaTime), transform.position.y,transform.position.z);
        }
    }

}
