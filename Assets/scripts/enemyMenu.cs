﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyMenu : MonoBehaviour
{
    private GameObject selectBon,turrelPositions,turrelPositions2;
    public bool bonusShieldBool, bonusSuperBulBool, bonusSuperTurBool;
    
        // Start is called before the first frame update
    void Start()
    {
        selectBon= GameObject.Find("selectBon");
        turrelPositions = selectBon.GetComponent<SelectBonus>().turel[0];
        turrelPositions2 = selectBon.GetComponent<SelectBonus>().turel[1];

    }

    // Update is called once per frame
    void Update()
    {
        bonusShieldBool =selectBon.GetComponent<SelectBonus>().bonusShieldBoll;
        bonusSuperBulBool =selectBon.GetComponent<SelectBonus>().bonusSuperBullBoll;
        bonusSuperTurBool =selectBon.GetComponent<SelectBonus>().bonusSuperTurBool;

        if(bonusShieldBool)
        transform.position = Vector3.MoveTowards(transform.position, turrelPositions.transform.position, Time.deltaTime);
        if(bonusSuperBulBool)
        transform.position = Vector3.MoveTowards(transform.position, turrelPositions2.transform.position, Time.deltaTime*0.5f);
        
    }
    void OnCollisionEnter (Collision enem)
    {
        if (enem.gameObject.name == "shield") {
            Destroy(gameObject);
            
        }
    }
}
