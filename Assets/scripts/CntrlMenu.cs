﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CntrlMenu : MonoBehaviour
{
    public GameObject bullet, fon, shopBGBonus, buttonnBonGO,buttonnFonGO;
    
    public GameObject turret,selectBon,turrelPositionsbonus;
    public GameObject[] guns;
       public float speedRotate;
    public Quaternion rotateGunMainMenu, rotateGunBonusSupperBull;
     public Vector3 posGunMenu;
     private Vector2 startPos;
     public float pos;
     public  Vector3 testquaternion;
     public Text maxScore, sumBullet,sumBullet2;
    // Start is called before the first frame update
    void Start()
    {
        
      StartCoroutine(BonusSupperBull());
      maxScore.text= PlayerPrefs.GetInt("MaxScore").ToString();
      
      
    //   PlayerPrefs.SetFloat("Shield", 10);
    }

    // Update is called once per framea
    void Update()
    {
        if(shopBGBonus.active==true)
        {
            
        selectBon= GameObject.Find("selectBon");
        turrelPositionsbonus = selectBon.GetComponent<SelectBonus>().turel[1];
        
        // 0.9510567, 0.9702958
        // -0.9702958,-0.9455187
        }
        testquaternion = guns[1].transform.position;
        turret.transform.Rotate(new Vector3(0,0,speedRotate));
        posGunMenu = guns[0].transform.position;
        rotateGunMainMenu = turret.transform.rotation;
        // if(Input.GetMouseButtonDown(0)) startPos = cam.ScreenToWorldPoint(Input.mousePosition);
        // else  if(Input.GetMouseButton(0))
        // {
        //     pos =cam.ScreenToWorldPoint(Input.mousePosition).x - startPos.x;
        //     cam.transform.position = new Vector3(transform.position.x-pos,transform.position.y,0);
        sumBullet.text= PlayerPrefs.GetInt("SumBullet").ToString();
        sumBullet2.text= PlayerPrefs.GetInt("SumBullet").ToString();
        // 5.308405, 5.629}
    }
    void OnMouseDown()
    {
        Instantiate(bullet,posGunMenu, rotateGunMainMenu);

    }
    public void ButtonPlay(){
        SceneManager.LoadScene(1);
    }


    public void ButtonBGBon()
    {
        shopBGBonus.SetActive(!shopBGBonus.activeSelf);
        Destroy(GameObject.FindGameObjectWithTag("Enemy"));
        Destroy(GameObject.FindWithTag("Bull"));
        buttonnBonGO.SetActive(!buttonnBonGO.activeSelf);
        buttonnFonGO.SetActive(!buttonnFonGO.activeSelf);
       
    }
    IEnumerator BonusSupperBull()
    {
        
        yield return new WaitUntil(() => selectBon.GetComponent<SelectBonus>().bonusSuperBullBoll == true);
        yield return new WaitForSeconds(8f);
        if(selectBon.GetComponent<SelectBonus>().bonusSuperBullBoll == true)
        {Instantiate(bullet,new Vector3(guns[1].transform.position.x,guns[1].transform.position.y,guns[1].transform.position.z+0.2f), turrelPositionsbonus.transform.rotation);
        }
        
        StartCoroutine(BonusSupperBull());
    }
    
}

