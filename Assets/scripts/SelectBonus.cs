﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectBonus : MonoBehaviour
{

public GameObject  enemy, buttonBuy, buttonLeft, buttonRoght,bonuses;
public GameObject[] turel;
public Vector3[] spawns;
public int randomSpawnInt, countSuperBulls,timeShield;
private float speedRotate =2;
private float timeSupperTurr;
public bool bonusShieldBoll, bonusSuperBullBoll,bonusSuperTurBool,  haveEnemy, buttonLeftBool,buttonRightBool;
private float speedRotateforSupTur;
public Text prise, sumBullet2, amountShield, amountSuppTurr;


    void OnTriggerEnter(Collider other)
        {
            if (other.name =="shield")
            {
                bonusShieldBoll=true;
                buttonBuy.SetActive(true);
                prise.text="100\nfor Shield";
                buttonRoght.SetActive(true);
                buttonLeft.SetActive(false);
                amountShield.text=PlayerPrefs.GetInt("Shield").ToString();
                
            }
            if(other.name == "SuperTur")
            {
                bonusSuperTurBool=true;
                buttonBuy.SetActive(true);
                prise.text="100\nfor Super Rotate";
                buttonRoght.SetActive(false);
                buttonLeft.SetActive(true);
                amountSuppTurr.text=PlayerPrefs.GetInt("timeSupperTurr").ToString();
               
                               
            }
            // Удаленная кнопка бонуса увеличение скорости вращения
            // if(other.name == "SuperTur")
            // {
            //     bonusSuperTurBool  = true; 
            //     buttonBuy.SetActive(true);
            //     prise.text="100\nfor second";
            //     buttonRoght1.SetActive(false);
            //     buttonRoght2.SetActive(false);
            //     buttonLeft1.SetActive(false);
            //     buttonLeft2.SetActive(true);
                
            // }
            buttonBuy.SetActive(true);
            
            // Debug.Log(other.name);

        }
     void OnTriggerExit(Collider other)
        {
            if (other.name =="shield")
            {
                bonusShieldBoll=false;
            }
            if(other.name == "SupBullTur")
            {
                bonusSuperBullBoll=false;
            }
            if(other.name == "SuperTur")
            {
             bonusSuperTurBool  = false; 
            }
        buttonBuy.SetActive(false);
        }
    
    // Start is called before the first frame update
    void Start()
    {
         speedRotateforSupTur =2;
        
    }

    // Update is called once per frame

   void Update()
    {
        // Кнопи перемотки
        if(buttonRightBool){bonuses.transform.position =Vector3.Lerp(bonuses.transform.position,new Vector3(5.3f, 
                bonuses.transform.position.y,bonuses.transform.position.z), Time.deltaTime*4f);}
        
        if(buttonLeftBool){bonuses.transform.position =Vector3.Lerp(bonuses.transform.position,new Vector3(10.3f, 
                bonuses.transform.position.y,bonuses.transform.position.z), Time.deltaTime*4f);}
       



        
        haveEnemy =GameObject.FindGameObjectWithTag("Enemy");
        
        if(bonusShieldBoll==true)
        {
        turel[0].transform.Rotate(new Vector3(0,0,speedRotate));
        
        }
        if(bonusSuperBullBoll==true)
        {
             buttonBuy.SetActive(true);
        // 
        }
        if(bonusSuperTurBool==true)
        {
        turel[2].transform.Rotate(new Vector3(0,0,speedRotateforSupTur));
        }
        else if (!bonusShieldBoll&&!bonusSuperBullBoll&&!bonusSuperTurBool)
        Destroy(GameObject.FindGameObjectWithTag("Enemy"));
    }
    IEnumerator spawnForShield(){
        yield return new  WaitUntil(() => haveEnemy == false && bonusShieldBoll ==true);
        for(int i=0;i<4;i++ ){
        if(bonusShieldBoll){
        Instantiate(enemy, spawns[i], Quaternion.identity);
        yield return new WaitForSeconds(1.5f);
        }
        }
        
        StartCoroutine(spawnForShield());
        
    } 
     IEnumerator BonusSuperBull(){
        yield return new  WaitUntil(() => haveEnemy == false && bonusSuperBullBoll ==true);
        
        if(bonusSuperBullBoll){
        Instantiate(enemy, spawns[4], Quaternion.identity);
        yield return new WaitForSeconds(2f);
        if(bonusSuperBullBoll)
        Instantiate(enemy, spawns[4], Quaternion.identity);
        yield return new WaitForSeconds(3.5f);
        
        }
        
        StartCoroutine(BonusSuperBull());
        
    } 
    IEnumerator BonusSuperTur(){
        yield return new WaitForSeconds(3f);
        speedRotateforSupTur = 10;
        yield return new WaitForSeconds(2);
        speedRotateforSupTur = 2;
            
        StartCoroutine(BonusSuperTur());
            }

    public void ButtonBonus()
    {
        StartCoroutine(spawnForShield());
        StartCoroutine(BonusSuperTur());
        StartCoroutine(BonusSuperBull());
    }
    public void ButtonLeft()
    {
        buttonLeftBool=true;
        buttonRightBool=false;
        
        Destroy(GameObject.FindWithTag("Bull"));
    }
    public void ButtonRight()
    {
        buttonRightBool=true;
        buttonLeftBool=false;
        Destroy(GameObject.FindWithTag("Bull"));
    }
    public void ButtonBuy()
    {
        if(bonusShieldBoll==true){
        if(PlayerPrefs.GetInt("SumBullet") >=100)
        {
           
            PlayerPrefs.SetInt("Shield",PlayerPrefs.GetInt("Shield")+1);
            PlayerPrefs.SetInt("SumBullet",PlayerPrefs.GetInt("SumBullet") -100);
            amountShield.text=PlayerPrefs.GetInt("Shield").ToString();
            
        }
        
        }

        if(bonusSuperBullBoll==true){
        if(PlayerPrefs.GetInt("SumBullet") >=100)
        {
           countSuperBulls= PlayerPrefs.GetInt("countSuperBulls");
            PlayerPrefs.SetInt("countSuperBulls",countSuperBulls+1);
            PlayerPrefs.SetInt("SumBullet",PlayerPrefs.GetInt("SumBullet") -100);
        }
        }


        if(bonusSuperTurBool  == true)
        {
            if(PlayerPrefs.GetInt("SumBullet") >=100)
            {
           
                PlayerPrefs.SetInt("timeSupperTurr",PlayerPrefs.GetInt("timeSupperTurr")+1);
                PlayerPrefs.SetInt("SumBullet",PlayerPrefs.GetInt("SumBullet") -100);
                amountSuppTurr.text=PlayerPrefs.GetInt("timeSupperTurr").ToString();
            }
            
        }

    sumBullet2.text=PlayerPrefs.GetInt("SumBullet").ToString();
        
    }






    // IEnumerator RightTObulls(){
        
    //     bonuses.transform.position =Vector3.Lerp(bonuses.transform.position,new Vector3(5.3f, 
    //     bonuses.transform.position.y,bonuses.transform.position.z), Time.deltaTime*4f);
    //     yield return new WaitForSeconds(2);
    //     Debug.Log("asd");
              
    // }
    // IEnumerator RightTOTur(){
        
    //     bonuses.transform.position =Vector3.Lerp(bonuses.transform.position,new Vector3(0.3f, 
    //     bonuses.transform.position.y,bonuses.transform.position.z), Time.deltaTime*4f);
    //     yield return new WaitForSeconds(2);
    //     Debug.Log("asd");
    // }
    // IEnumerator LEdtTObulls(){
        
    //     bonuses.transform.position =Vector3.Lerp(bonuses.transform.position,new Vector3(5.3f, 
    //     bonuses.transform.position.y,bonuses.transform.position.z), Time.deltaTime*4f);
    //     yield return new WaitForSeconds(2);
    //     Debug.Log("asd");
        
    //     }


    //  IEnumerator LEdtTOShield(){
        
    //     bonuses.transform.position =Vector3.Lerp(bonuses.transform.position,new Vector3(10.3f, 
    //     bonuses.transform.position.y,bonuses.transform.position.z), Time.deltaTime*4f);
    //     yield return new WaitForSeconds(2);
    //     Debug.Log("asd");
    //     }


}
