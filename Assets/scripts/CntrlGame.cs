﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class CntrlGame : MonoBehaviour
{
    
    public GameObject turret, gun, loseScreen  ;
    public GameObject[] turrel,gunsEndGame;
    public Camera mainCamera;
    public Transform turrelPosition;
    public float waitToSpawn;
    public float speedBull, speedEnemy;
    public GameObject bullet, enemy;
    public Vector3 posGun;
    public Quaternion rotateGun;
    public bool loose,leftMoveTurrelBool,RightMoveTurrelBool;
    public Vector3[] spawn;
    public int randomSpawnInt,randomSwitchRoom, countBullet, numberFakeMove, sumBullet;
    public Text textBulletCount,resultCount ;
    public bool _spiral, boolEnemy, _frequancyUp, _frequancyLeft, _fakeMoveEnemy, mouseOn, bonusSuperSuperTurBool;
    public Color   colorEnd, testColor;
    

    public float hue, S, V, deltaHue, mousePositionX,mousePositionY;
    private float timeDelta;

    public Vector3  MousePos;
    
    void Start()
    {
       
        
        loseScreen.SetActive(false);
        testColor =new Color(0, 1, 0, 1);
        // snakeContt.active = false;
        mainCamera.GetComponent<Camera>().clearFlags = CameraClearFlags.SolidColor;
        randomSpawnInt = Random.Range(0, 16);
        // Color.RGBToHSV(mainCamera.backgroundColor, out hue, out S, out V);
       
        StartCoroutine(spawnEnemy());
        StartCoroutine(loseGAme() );
        // if(PlayerPrefs.GetFloat("Shield")>0)
        // {
        //     buttonShield.SetActive(true);
        // }
        // else 
        //      {
        //     bonusShieldGameObject.SetActive(false);
        //     buttonShield.SetActive(false);
        //     }
        // if(PlayerPrefs.GetInt("countSuperBulls")>0)
        // {
        //     buttonSupperBull.SetActive(true);
        // }
        // else buttonSupperBull.SetActive(false);
        // if(PlayerPrefs.GetFloat("timeSupperTurr")>0)
        // {
        //     buttonSuperTur.SetActive(true);
        // }
        // else buttonSuperTur.SetActive(false);


    }

    // Update is called once per frame
    void Update()
    {


        bonusSuperSuperTurBool= GameObject.Find("Turret").GetComponent<BonusInGame>().bonusSuperSuperTurBool;
        // Debug.Log(Time.deltaTime);
        timeDelta += (Time.deltaTime / 100);
        if(bonusSuperSuperTurBool)
            turret.transform.Rotate(new Vector3(0,0,6));
        else turret.transform.Rotate(new Vector3(0,0,2.5f));
        posGun = gun.transform.position;
        rotateGun = turret.transform.rotation;
        boolEnemy =GameObject.FindGameObjectWithTag("Enemy");
        mouseOn = Input.GetKeyDown(KeyCode.Mouse0);
    //    Camera.main.backgroundColor= Color.Lerp(Camera.main.backgroundColor,colorEnd, Time.deltaTime/5);
        if(leftMoveTurrelBool)
        {
            turrel[0].transform.rotation = new Quaternion(turrel[0].transform.rotation.x,turrel[0].transform.rotation.y,
        Mathf.Lerp(turrel[0].transform.rotation.z,turrel[0].transform.rotation.z+30,Time.deltaTime/50),turrel[0].transform.rotation.w);
            turrel[1].transform.rotation = new Quaternion(turrel[1].transform.rotation.x,turrel[1].transform.rotation.y,
        Mathf.Lerp(turrel[1].transform.rotation.z,turrel[1].transform.rotation.z+30,Time.deltaTime/50),turrel[1].transform.rotation.w);

        }
         if(RightMoveTurrelBool)
        {
            turrel[0].transform.rotation = new Quaternion(turrel[0].transform.rotation.x,turrel[0].transform.rotation.y,
        Mathf.Lerp(turrel[0].transform.rotation.z,turrel[0].transform.rotation.z-30,Time.deltaTime/50),turrel[0].transform.rotation.w);
            turrel[1].transform.rotation = new Quaternion(turrel[1].transform.rotation.x,turrel[1].transform.rotation.y,
        Mathf.Lerp(turrel[1].transform.rotation.z,turrel[1].transform.rotation.z-30,Time.deltaTime/50),turrel[1].transform.rotation.w);

        }

        // Текст на кнопках бонусах
        
    
                
    }

            void OnMouseDown()
    {
        if(countBullet > 0)
        {
        countBullet = countBullet -1;
        textBulletCount.text=countBullet.ToString();
        Instantiate(bullet,posGun, rotateGun);
        MousePos = Input.mousePosition;
        mousePositionX =MousePos.x/79-3;
        mousePositionY = MousePos.y/79-5;
        }
        
    }
    
 

    IEnumerator spawnEnemy() 
    {       

        
            yield return new WaitUntil(() => boolEnemy == false);
            yield return new WaitUntil(() => loose == false);
            //deltaHue=hue+50;
       
       randomSwitchRoom = Random.Range(0, 14);    
            // randomSwitchRoom =14;  

            //color1= new Color(hue,S,V);
            //color2= new Color(deltaHue,S,V);
           

            //mainCamera.backgroundColor = Color.Lerp(color1, color2, Mathf.PingPong(Time.time, 1));
           // hue=deltaHue;
           _fakeMoveEnemy = false;
        //    snakeContt.active = false;
            _spiral =false;
            _frequancyLeft = false;
            _frequancyUp = false;
            switch(randomSwitchRoom)
            {
            case 0:      
            Instantiate(enemy, spawn[randomSpawnInt], Quaternion.identity);
           
            randomSpawnInt = Random.Range(0, 16);
            yield return new WaitForSeconds(waitToSpawn);
            colorEnd=new Color(0.8017052f,1,0.6f);
            

            StartCoroutine(spawnEnemy());  
               
             
            break;

            case 1:
            for(int i=0;i<16;i++ ){
                yield return new WaitForSeconds(waitToSpawn/2.5f);
                Instantiate(enemy, spawn[i], Quaternion.identity);
            }
            colorEnd=new Color(1,0.967803f,0.6f);
           

            StartCoroutine(spawnEnemy());
            break;
            
            case 2:
            for(int i=0;i<16;i++ ){
                
                Instantiate(enemy, spawn[i], Quaternion.identity);
               
            }
             yield return new WaitForSeconds(waitToSpawn*2);
              colorEnd=new Color(1,0.7618561f,0.6f);
           
            StartCoroutine(spawnEnemy());
            break;
            case 3:
            for(int i=0;i<=4;i++ ){
                
                Instantiate(enemy, spawn[i], Quaternion.identity);
            
            }
             yield return new WaitForSeconds(waitToSpawn*2);
               colorEnd=new Color(1,0.6040414f,0.6f);
            
            StartCoroutine(spawnEnemy());

            break;
            case 4:
            for(int i=4;i<=8;i++ ){
                
                Instantiate(enemy, spawn[i], Quaternion.identity);
                
               
            }
             yield return new WaitForSeconds(waitToSpawn*2);
               colorEnd=new Color(1,0.6f,0.8363886f);
          
            StartCoroutine(spawnEnemy());

            break;
            case 5:
            for(int i=8;i<=12;i++ ){
                
                Instantiate(enemy, spawn[i], Quaternion.identity);
               
            }
             yield return new WaitForSeconds(waitToSpawn*2);
             colorEnd=new Color(0.9029754f,0.6f,1);
          ;
           StartCoroutine(spawnEnemy());

            break;
            case 6:
            for(int i=12;i<16;i++ ){
                
                Instantiate(enemy, spawn[i], Quaternion.identity);
               
            }
            Instantiate(enemy, spawn[0], Quaternion.identity);

             yield return new WaitForSeconds(waitToSpawn*2);
             colorEnd=new Color(0.7318209f,0.6f,1);
           
             StartCoroutine(spawnEnemy());
            

            break;

            case 7:
            for(int i=12;i<16;i++ ){
                
                Instantiate(enemy, spawn[i], Quaternion.identity);
               
            }
            Instantiate(enemy, spawn[0], Quaternion.identity);
            colorEnd=new Color(0.6f,0.7107068f,1);
             yield return new WaitForSeconds(waitToSpawn*2);
            StartCoroutine(spawnEnemy());
            

            break;
            case 8:
            _spiral = true;
            for(float i=0;i<=7;i=i+0.5f )
            {
            Instantiate(enemy, spawn[0], Quaternion.identity);

            enemy.GetComponent<enemy>().speedSpiral = i;
            
            }
            yield return new WaitForSeconds(waitToSpawn*2);
            colorEnd=new Color(0.6f,0.9460231f,1);
            StartCoroutine(spawnEnemy());
           
            break;

                 case 9:
            _frequancyUp = true;
            for(int i=4;i<=8;i++ ){
                
                Instantiate(enemy, spawn[i], Quaternion.identity);
                
               
            }
             yield return new WaitForSeconds(waitToSpawn*2);
             colorEnd=new Color(0.6f,1,0.8255278f);
            StartCoroutine(spawnEnemy());
           
            break;
            case 10:
             _frequancyUp = true;
            for(int i=12;i<16;i++ ){
                
                Instantiate(enemy, spawn[i], Quaternion.identity);
               
            }
            Instantiate(enemy, spawn[0], Quaternion.identity);

             yield return new WaitForSeconds(waitToSpawn*2);
             colorEnd=new Color(0.6f,1,0.6634105f);
             StartCoroutine(spawnEnemy());
            

            break;
            case 11:
            _frequancyLeft = true;
            for(int i=0;i<=4;i++ ){
                
                Instantiate(enemy, spawn[i], Quaternion.identity);
            
            }
             yield return new WaitForSeconds(waitToSpawn*2);
             colorEnd=new Color(0.6847505f,1,0.6f);
            StartCoroutine(spawnEnemy());

            break;
            case 12:
            _frequancyLeft = true;
            for(int i=8;i<=12;i++ ){
                
                Instantiate(enemy, spawn[i], Quaternion.identity);
               
            }
             yield return new WaitForSeconds(waitToSpawn*2);
             colorEnd=new Color(0.9347351f,1,0.6f);
           StartCoroutine(spawnEnemy());

            break;
            case 13:
            
            Instantiate(enemy, spawn[Random.Range(0, 12)], Quaternion.identity);
            
            yield return new WaitUntil(() => mouseOn == true);
;
            _fakeMoveEnemy = true;
            yield return new WaitForSeconds(waitToSpawn*2);
             colorEnd=new Color(1,0.6440474f,0.6f);
           StartCoroutine(spawnEnemy());

            break;

             case 14:
          numberFakeMove=0;
          while(numberFakeMove<10)
          {
               _fakeMoveEnemy = true;
                Instantiate(enemy, spawn[Random.Range(0, 12)], Quaternion.identity);
                yield return new WaitForSeconds(4f);
                numberFakeMove++;
          }
           
            yield return new WaitForSeconds(waitToSpawn*2);
            StartCoroutine(spawnEnemy());
            break;

            // запуск Змейки
            //      case 13:

            //     snakeContt.active = true;
            //     yield return new WaitForSeconds(2);
            //     StartCoroutine(spawnEnemy());



            // break;

            }

        

       

    }
    
    IEnumerator loseGAme() 
    {  
        yield return new WaitUntil(() => loose == true);
        Handheld.Vibrate();
        Destroy(GameObject.FindGameObjectWithTag("Enemy"));
        Destroy(GameObject.FindWithTag("Bull"));
        loseScreen.SetActive(true);
        resultCount.text = countBullet.ToString();
        
        sumBullet= PlayerPrefs.GetInt("SumBullet");
        PlayerPrefs.SetInt("SumBullet", sumBullet+countBullet);
        
        if(PlayerPrefs.GetInt("MaxScore")<countBullet)
        {
            PlayerPrefs.SetInt("MaxScore", countBullet);
        }
        StartCoroutine(MoveTurrelEndGame());
        StartCoroutine(InstantiateBullEndGame());
        StartCoroutine(InstantiateEnemyEndGame());
     }

     IEnumerator MoveTurrelEndGame() 
    {  
        leftMoveTurrelBool = true;
        RightMoveTurrelBool = false;
        yield return new WaitForSeconds(2);
        leftMoveTurrelBool = false;
        RightMoveTurrelBool = true;
        yield return new WaitForSeconds(2);
        StartCoroutine(MoveTurrelEndGame());

     }
     IEnumerator InstantiateBullEndGame() 
    { 
        yield return new WaitUntil(() => loose == true);
        yield return new WaitForSeconds(0.5f);
        Instantiate(bullet,new Vector3(gunsEndGame[0].transform.position.x,gunsEndGame[0].transform.position.y,gunsEndGame[0].transform.position.z+0.1f),
            turrel[0].transform.rotation);
        Instantiate(bullet,new Vector3(gunsEndGame[1].transform.position.x,gunsEndGame[1].transform.position.y,gunsEndGame[1].transform.position.z+0.1f),
            turrel[1].transform.rotation);
        StartCoroutine(InstantiateBullEndGame());
    }

      IEnumerator InstantiateEnemyEndGame() 
    {  
        yield return new WaitForSeconds(1.5f);
        for(int i=17;i<=21;i++ )
            {
                Instantiate(enemy, spawn[i], Quaternion.identity);
            }
        StartCoroutine(InstantiateEnemyEndGame());
     }
public void ButtonAgain(){
        SceneManager.LoadScene(1);
    }
    public void ButtonBack(){
        SceneManager.LoadScene(0);
    }

    
        
}

   
        

 



