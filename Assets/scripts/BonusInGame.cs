﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BonusInGame : MonoBehaviour
{
    public float firstClickTime,timeBetweenClick, holdTime;
    private int clickCounter;
    private bool coroutineAllowed,bonusSieldBool, holdMouseBool;
    public GameObject bonusShieldGameObject, timerBonusShield,timerBonusSuperTur,lightForSuperTur;
 
    public Text numberShields,numberSSuperTur;
    private Renderer rend;
    // public Light    lightForSuperBulls;
    public bool bonusSuperSuperTurBool;
  
    // Start is called before the first frame update
    void Start()
    {
       holdMouseBool=false;
        firstClickTime=0f;
        timeBetweenClick = 0.2f;
        clickCounter = 0;
        coroutineAllowed = true;
        bonusShieldGameObject.SetActive(false);
        bonusSieldBool =false;
        rend = GetComponent<Renderer> ();
    }

    // Update is called once per frame
    void Update()
    {
        
       
        
        
        // Бонус Щит
       if(bonusSieldBool)
       {
           if(timerBonusShield.transform.position.x>-6.8f)
        {
            timerBonusShield.transform.position = Vector3.MoveTowards(
                timerBonusShield.transform.position,
                new Vector3(-6.8f,timerBonusShield.transform.position.y,timerBonusShield.transform.position.z),
                Time.deltaTime*0.4f);
        }
        if(timerBonusShield.transform.position.x<-6.7f)
        {
            
            
            bonusShieldGameObject.SetActive(false);
            bonusSieldBool=false;
        }

       }

       // Бонус Супер Турель
         if(bonusSuperSuperTurBool)
       {
           if(timerBonusSuperTur.transform.position.x>-6.8f)
        {
            timerBonusSuperTur.transform.position = Vector3.MoveTowards(
                timerBonusSuperTur.transform.position,
                new Vector3(-6.8f,timerBonusSuperTur.transform.position.y,timerBonusSuperTur.transform.position.z),
                Time.deltaTime*0.4f);
        }
        if(timerBonusSuperTur.transform.position.x<-6.7f)
        {
            
            
            lightForSuperTur.SetActive(false);
            bonusSuperSuperTurBool=false;
        }

       }



        if (clickCounter != 1 || !coroutineAllowed)
        {
        }
        else
        {
            firstClickTime = Time.time;
            StartCoroutine(DoubleClickDetection());

        }
        
        if (holdMouseBool==true)
            holdTime+=Time.deltaTime;        
        if(holdTime>0.5f)
        {
            holdMouseBool=false;
            StartCoroutine(LongClick());
            holdTime=0;
        }
    if(GameObject.Find("GameCntrl").GetComponent<CntrlGame>().loose==true)
    {

        bonusShieldGameObject.SetActive(false);
        lightForSuperTur.SetActive(false);

    }
    }
    void OnMouseDown()
    {
       holdMouseBool=true;
        holdTime+=Time.deltaTime;
       
        
    }
    
    void OnMouseUp()
    {
        // StopCoroutine(LongClickDetection());
        holdMouseBool=false;
        holdTime=0;
        clickCounter +=1;
        
    }
     private IEnumerator DoubleClickDetection()
    {
        coroutineAllowed = false;
        while(Time.time<firstClickTime+timeBetweenClick)
        {
            if(clickCounter==2)
            {
                
                 if(PlayerPrefs.GetInt("Shield")>0)
                 {
                    Handheld.Vibrate();
                    bonusSieldBool =true;
                    timerBonusShield.transform.position = new Vector3(0, timerBonusShield.transform.position.y,timerBonusShield.transform.position.z);
                    bonusShieldGameObject.SetActive(true);
                    PlayerPrefs.SetInt("Shield", PlayerPrefs.GetInt("Shield")-1);
                    numberShields.text = PlayerPrefs.GetInt("Shield").ToString();
                    break;
                 }
            }
            yield return new WaitForEndOfFrame();
        }
        clickCounter=0;
        firstClickTime=0f;
        coroutineAllowed = true;
    }
    private IEnumerator LongClick()
    {
        if(PlayerPrefs.GetInt("timeSupperTurr")>0)
        {
            timerBonusSuperTur.transform.position = new Vector3(0, timerBonusShield.transform.position.y,timerBonusShield.transform.position.z);
            bonusSuperSuperTurBool=true;
            lightForSuperTur.SetActive(true);
            PlayerPrefs.SetInt("timeSupperTurr", PlayerPrefs.GetInt("timeSupperTurr")-1);
            numberSSuperTur.text = PlayerPrefs.GetInt("timeSupperTurr").ToString();
            Debug.Log("LOOOOOOONG");
            // PlayerPrefs.SetInt("countSuperBulls",countSuperBulls+1);
            
        }
        else bonusSuperSuperTurBool=false;
        
        yield return new WaitForSeconds(1.5f);
    
        
    }

}

