﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakeControl : MonoBehaviour
{

    public List<Transform> Tails;
    [Range(0, 3)]
    public float bonesDistans;
    public GameObject bonePrefab;
    private Transform _transform;
    public float speed;
    public int direction;
    
    public Transform food1, food2,food3;
    public bool f1,f2,f3;
    public bool next1, next2,next3;
    
    // Start is called before the first frame update
    void Start()
    {
        // _transform = GetComponent<Transform>();
        // StartCoroutine(ternSnake());
        // StartCoroutine(corMoveSnake());
    }

    // Update is called once per frame
    void Update()
    {
        // Тут условие появление Змеюки
        // if(next1 ==true){
        //     MoveSnake(_transform.position + Vector3.up* speed);
        // }

       /* f1=GameObject.Find("food1");
       
       if(System.Math.Round(food1.position.y,1)==System.Math.Round(gameObject.GetComponent<Transform>().position.y,1)){next1=true;} 

       if ((GameObject.Find("food1")==true)&&(food1.position.y>=gameObject.GetComponent<Transform>().position.y))
       {
           MoveSnake(_transform.position + Vector3.up* speed);
       }
       if ((GameObject.Find("food1")==true)&&(food1.position.x<gameObject.GetComponent<Transform>().position.x)&& (next1==true))
       {
           MoveSnake(_transform.position + Vector3.left* speed);
       }
         if ((GameObject.Find("food1")==false)&&(food2.position.y>=gameObject.GetComponent<Transform>().position.y))
         {
            MoveSnake(_transform.position + Vector3.up* speed);
         }

        
         switch(direction)
        {
            case 0:
            StartCoroutine(ternSnake());
            MoveSnake(_transform.position + Vector3.up* speed);
            break;
            case 1:
           // _transform.rotation.z = 90;
            MoveSnake(_transform.position + Vector3.left* speed);
            break;
            case 2:
            MoveSnake(_transform.position + Vector3.up* speed);
            break;
            case 3:
            MoveSnake(_transform.position + Vector3.right* speed);
            break;
            case 4:
            MoveSnake(_transform.position + Vector3.down* speed);
            break;
            case 5:
            MoveSnake(_transform.position + Vector3.left* speed);
            break;
             case 6:
            MoveSnake(_transform.position + Vector3.up* speed);
            break;
        }
        */
    }
     private void MoveSnake(Vector3 newPosition)
     {
         float sqrDistance = bonesDistans* bonesDistans;
         Vector3 previousPosition = _transform.position;

         foreach (var bone in Tails)
         {
             if ((bone.position - previousPosition).sqrMagnitude > sqrDistance )
             {

                 var temp = bone.position;
                 bone.position = previousPosition;
                 previousPosition = temp;
             }
             else{
                 break;
             }
         }


         _transform.position = newPosition;
     }


    IEnumerator corMoveSnake()
    {   
        next1=true;
        
         
        yield return new WaitUntil(() =>System.Math.Round(food1.position.y,1)==System.Math.Round(gameObject.GetComponent<Transform>().position.y,1));
        MoveSnake(_transform.position + Vector3.left* speed);
        yield return new WaitUntil(() =>System.Math.Round(food1.position.x,1)==System.Math.Round(gameObject.GetComponent<Transform>().position.x,1));

    }

    IEnumerator ternSnake()
    {   
        direction = 0;
        yield return new WaitForSeconds(1.5f);
        direction = 1;
        yield return new WaitForSeconds(1f);
        direction = 2;  
        yield return new WaitForSeconds(4f);
        direction = 3;
        yield return new WaitForSeconds(2.3f);
        direction = 4;
        yield return new WaitForSeconds(4);
        direction = 5;
        yield return new WaitForSeconds(1);
         direction = 6;
        yield return new WaitForSeconds(4);
        

    }
    private void OnCollisionEnter(Collision collicion)
    {
        if(collicion.gameObject.tag == "food")
        {
            Destroy(collicion.gameObject);
            var bone = Instantiate(bonePrefab);
            Tails.Add(bone.transform);
        }
        
    }


}

