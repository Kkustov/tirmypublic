using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class enemy : MonoBehaviour
{
    // Start is called before the first frame update

    public Transform turrelPositions;
    public GameObject gameOb;
    public float speedEnemy;
    public float frequancy,speedSpiral;
    public float magnitude, counterTime;
    public Vector3 sineMove,endtLocation, addVector;

   

    
    void Start()
    {
        
    gameOb= GameObject.Find("GameCntrl");
    turrelPositions = gameOb.GetComponent<CntrlGame>().turrelPosition;
    speedEnemy = gameOb.GetComponent<CntrlGame>().speedEnemy;
    
    //Debug.Log((transform.up * Mathf.Sin(Time.time * frequancy) * magnitude));
    endtLocation = transform.position;
    if(gameOb.GetComponent<CntrlGame>().loose)
    {Destroy(gameObject,20);}
    }

    // Update is called once per frame
    void Update()
    {
        
        if(!gameOb.GetComponent<CntrlGame>().loose){
        counterTime+=Time.deltaTime;
       transform.position = Vector3.MoveTowards(transform.position, turrelPositions.position, Time.deltaTime*speedEnemy);
        }
    //Колебания вверх transform.position = transform.position + transform.up * Mathf.Sin(Time.time * frequancy) * magnitude;
       //  transform.position += Mathf.Pow(2, x/2);
       if( gameOb.GetComponent<CntrlGame>()._spiral == true)
       {
        speedSpiral += 0.03f;
        float x = (float)Mathf.Sin(speedSpiral)/(speedSpiral/(40-(speedSpiral)));
        float y = (float)Mathf.Cos(speedSpiral)/(speedSpiral/(40-(speedSpiral)));
        float z = -1.32f;
        addVector = new Vector3(x, y, z);
        transform.position = addVector;
        
       }
       if (gameOb.GetComponent<CntrlGame>()._frequancyLeft == true)
       {
        transform.position = transform.position + transform.right * Mathf.Sin(Time.time * frequancy) * magnitude;
       }
        if (gameOb.GetComponent<CntrlGame>()._frequancyUp == true)
       {
        transform.position = transform.position + transform.up * Mathf.Sin(Time.time * frequancy) * magnitude;
       }
       if(gameOb.GetComponent<CntrlGame>()._fakeMoveEnemy == true)
       {
           transform.position = new Vector3(gameOb.GetComponent<CntrlGame>().mousePositionX,gameOb.GetComponent<CntrlGame>().mousePositionY, -1.32f);
            gameOb.GetComponent<CntrlGame>()._fakeMoveEnemy = false;
       }
       if(gameOb.GetComponent<CntrlGame>().loose){
        transform.position = Vector3.MoveTowards(transform.position, 
        new Vector3(0,gameOb.GetComponent<CntrlGame>().turrel[0].transform.position.y,gameOb.GetComponent<CntrlGame>().turrel[0].transform.position.z) ,
        Time.deltaTime*speedEnemy);
       }
     }

    void OnCollisionEnter (Collision enem)
    {
        if (enem.gameObject.name == "Bullet(Clone)") 
        {
            gameOb.GetComponent<CntrlGame>().countBullet = gameOb.GetComponent<CntrlGame>().countBullet+2;
            gameOb.GetComponent<CntrlGame>().textBulletCount.text = gameOb.GetComponent<CntrlGame>().countBullet.ToString();
                 
        }
        if (enem.gameObject.tag == "Player")
         {
            gameOb.GetComponent<CntrlGame>().loose =true;
            Destroy(gameObject);
                        
        }
         if (enem.gameObject.name == "shield")
         {
            Destroy(gameObject);
            
        }
    }


}


